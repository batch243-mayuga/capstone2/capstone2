const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth")


module.exports.checkEmailExists = (request, response, next) => {
    return User.find({email: request.body.email}).then(result => {
        let message = ``;
        if(result.length > 0){
            message = `The mail ${request.email} is already taken. Please use other email.`;
            return response.send({emailAlreadyExists: true});

        }else{
            next();
        }
    })
}

// User Registration
module.exports.registerUser = (request, response) => {

	let newUser = new User ({
		fullName: request.body.fullName,
		email: request.body.email,
		password: bcrypt.hashSync(request.body.password, 10),
		mobileNo: request.body.mobileNo
	})

	let mobileNoLength;
	
	if(newUser.mobileNo.length >= 11){
		let registered;

		return newUser.save().then(user => {
			console.log(user);
			response.send({registered: true})
	}).catch(error =>{
		console.log(error);
		response.send({registered: false})
	})
	} else {
		response.send({registered: false, mobileNoLength: false})
	}
}



// User Authentication
module.exports.loginUser = (req, res) => {

	return User.findOne({email: req.body.email}).then(result => {
		console.log(result);

		if(result === null){
			res.send({accessToken: 'empty'})
		} else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect){

				return res.send({accessToken: auth.createAccessToken(result)});
			} else{
				return res.send({accessToken: 'empty'});
			}
		}
	})
}





// Update role
module.exports.updateRole = (req, res) => {
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	let idToBeUpdated = req.params.userId;

	if(userData.isAdmin){

		return User.findById(idToBeUpdated).then(result => {

			let update = {
				isAdmin: !result.isAdmin
			};

			return User.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(document => {
				document.password = "Confidential";
				return res.send(document)}).catch(err => res.send(err));
			
		}).catch(err => res.send(err));

	} else{
		return res.send("You don't have access on this page!")
	}
}


module.exports.profileDetails = (request, response) =>{
	// user will be object that contains the id and email of the user that is currently logged in.
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);
	

	return User.findById(userData.id).then(result => {
		result.password = "Confidential";
		return response.send(result)
	}).catch(err => {
		return response.send(err);
	})
	
}
