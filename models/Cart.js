const mongoose = require("mongoose")

const Schema = mongoose.Schema

const Cart = new Schema({
    product: {
        type: mongoose.Types.ObjectId,
        ref: "Product",
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: "User",
    },
    quantity: Number,
})

module.exports = mongoose.model("Cart", Cart)