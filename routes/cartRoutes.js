const express = require("express")
const router = express.Router()
const auth = require("../auth")

const cartControllers = require("../controllers/cartControllers")


router.post("/:productId", auth.verify, cartControllers.addItem)


module.exports = router
