const express = require("express");
const router = express.Router();

const auth = require("../auth");

const userControllers = require("../controllers/userControllers");

	

	router.post("/register", userControllers.checkEmailExists ,userControllers.registerUser);

	router.post("/login", userControllers.loginUser);

	router.get("/profile", userControllers.profileDetails);

	router.patch("/updateRole/:userId", auth.verify, userControllers.updateRole);

	

module.exports = router;